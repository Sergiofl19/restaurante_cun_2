<div class="container" >
	<!-- Top Navigation --> 
	<div class="codrops-top clearfix">
		<a class="codrops-icon codrops-icon-prev" href="?c=Index&m=indexA"><span><font size="3" face="Cooper Black"> Regresar</font></span></a>

	</div>
		<header>
			<h1><font size="10" face="Cooper Black" >Modulo de Administración</font><span>Reportes de Información en el Sistema</span></h1>	
 		</header>
			
		<div class="component">

			<ul class="align">
				<li>
					<figure class='book'>
 						<!-- Front -->
						<ul class='hardcover_front'>
							<li><span class="ribbon">Nº1</span>
								<img src="<?php echo URL_ASSETS ?>imagenes/usuario.jpg" alt="" width="100%" height="100%">
							</li>
							<li>
								
							</li>
						</ul>
						<!-- Pages -->
						<ul class='page'>
							<li></li>
							<li>
								<a class="btn" href="ReporteUsuarios.php">Descargar</a>
							</li>
							<li></li>
							<li></li>
							<li></li>
						</ul>
						<!-- Back -->
						<ul class='hardcover_back'>
							<li></li>
							<li></li>
						</ul>
						<ul class='book_spine'>
							<li></li>
							<li></li>
						</ul>
						<figcaption>
							<h1><font size="6" face="Cooper Black" >Usuarios en el Sistema</font></h1>
							<span>Svet-cli</span>
								<p align="justify">En este reporte encontrara el listado de los usuarios registrados en el sistema con sus respectivos datos y rol que ocupa en el sistema.</p>
						</figcaption>
					</figure>
				</li>

					<li>
					<figure class='book'>
						<!-- Front -->
						<ul class='hardcover_front'>
							<li><span class="ribbon">Nº2</span>
								<img src="<?php echo URL_ASSETS ?>imagenes/Propietario.jpg" alt="" width="100%" height="100%">
							</li>
							<li>
								
							</li>
						</ul>
						<!-- Pages -->
						<ul class='page'>
							<li></li>
							<li>
								<a class="btn" href="reportePropietario.php">Descargar</a>
							</li>
							<li></li>
							<li></li>
							<li></li>
						</ul>
						<!-- Back -->
						<ul class='hardcover_back'>
							<li></li>
							<li></li>
						</ul>
						<ul class='book_spine'>
							<li></li>
							<li></li>
						</ul>
							<figcaption>
							<h1><font size="6" face="Cooper Black" >Propietarios de Pacientes en el Sistema</font></h1>
							<span>Svet-cli</span>
								<p align="justify">En este reporte encontrara el listado de los Propietarios de pacientes que cuentan con una historia clinica registrada en el sistema con sus respectivos datos.</p>
						</figcaption>
					</figure>
					</li>

					<li>
						<figure class='book'>
						<!-- Front -->
						<ul class='hardcover_front'>
							<li><span class="ribbon">Nº3</span>
								<img src="<?php echo URL_ASSETS ?>imagenes/Pacientes.jpg" alt="" width="100%" height="100%">
							</li>
							<li>
								
							</li>
						</ul>
						<!-- Pages -->
						<ul class='page'>
							<li></li>
							<li>
								<a class="btn" href="reporte.php">Descargar</a>
							</li>
							<li></li>
							<li></li>
							<li></li>
						</ul>
						<!-- Back -->
						<ul class='hardcover_back'>
							<li></li>
							<li></li>
						</ul>
						<ul class='book_spine'>
							<li></li>
							<li></li>
						</ul>
						<figcaption>
							<h1><font size="6" face="Cooper Black">Pacientes registrados en el ultimo mes</font></h1>
								<span>Svet-cli</span>
							<p align="justify">En este reporte encontrara el listado de los Pacientes a los cuales se les registro una historia clinica en el istema en el transcurso del ultimo mes.</p>
							</figcaption>
						</figure>
					/li>
					<li>
						<figure class='book'>
						<!-- Front -->
							<ul class='hardcover_front'>
								<li><span class="ribbon">Nº4</span>
									<img src="<?php echo URL_ASSETS ?>imagenes/Razas.jpg" alt="" width="100%" height="100%">
								</li>
								<li>
									
								</li>
							</ul>
						<!-- Pages -->
							<ul class='page'>
								<li></li>
								<li>
									<a class="btn" href="reporteRaza.php">Descargar</a>
								</li>
								<li></li>
								<li></li>
								<li></li>
							</ul>
						<!-- Back -->
							<ul class='hardcover_back'>
								<li></li>
								<li></li>
							</ul>
							<ul class='book_spine'>
								<li></li>
								<li></li>
							</ul>
							<figcaption >
								<h1><font size="6" face="Cooper Black">Razas y Especies en Tratamiento</font></h1>
								<span>Svet-cli</span>
								<p align="justify">En este reporte encontrara el listado de las Razas y Especies de Pacientes en tratamiento a los cuales se les registro una historia clinica en el sistema.</p>
							</figcaption>
						</figure>
					</li>
				</ul>
			
				<div class="codrops-top clearfix">
					<a class="codrops-icon codrops-icon-prev" href="?c=Index&m=indexA"><span><font size="3" face="Cooper Black"> Regresar</font></span></a>
				</div>
			</div>
</div><!-- /container -->