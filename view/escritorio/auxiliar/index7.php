<ul class="cb-slideshow">
    <li><span>Image 01</span><div><h3>Svet-cli</h3></div></li>
    <li><span>Image 02</span><div><h3>ASMAXI</h3></div></li>
    <li><span>Image 03</span><div><h3>Seguridad</h3></div></li>
    <li><span>Image 04</span><div><h3><FONT SIZE="20">Es Imprescindible para tu trabajo</font></h3></div></li>
    <li><span>Image 05</span><div><h3>Es una Herramienta</font></h3></div></li>
    <li><span>Image 06</span><div><h3>Creatividad</h3></div></li>
</ul>
<style>

    li div h3{ text-shadow: -1px -1px 1px #fff, 1px 1px 1px #fff, -1px 1px 1px #fff, 1px -1px 1px #fff;
    -webkit-text-fill-color: #000;
    -webkit-text-stroke: 3px normal;
    }

    h2 p{
    text-shadow: -1px -1px 1px #fff, 1px 1px 1px #fff, -1px 1px 1px #fff, 1px -1px 1px #fff;
    -webkit-text-fill-color: #000;
    -webkit-text-stroke: 3px normal;
   }  
</style>
    <div class="container">
        <!-- Codrops top bar -->
        <div class="codrops-top">
            <a href="?c=Index&m=indexA">
                <strong>&laquo;<font size="3" face="Cooper Black"> Regresar</font></strong>
                </a>
            <div class="clr"></div>
        </div><!--/ Codrops top bar -->
            <header>
                <h1><img src="<?php echo URL_ASSETS ?>imagenes/Svet-cli.PNG"></h1>

                <div align="justify" ><h2><p><font size="5"> 
                        Svet-cli es un software (sistema de información)<br>
                        creado con el fin de  encargarse de  la  gestión<br>  
                        del historial clínico de la clínica veterinaria.<br>
                        Permite de manera rápida el ingreso, validación,<br>
                        consulta y  almacenamiento  de  la  información <br>
                        correspondiente   a   Propietarios,   Pacientes,<br>
                        Personal clínico, historial clínico,diagnósticos<br> 
                        y chequeo de pacientes.<br>  
                        Además su funcionamiento  le  evita  pérdida  de<br>
                        tiempo recopilando información ya que el sistema<br> 
                        busca la información solicitada que se encuentra<br> 
                        almacenada en la base  de datos demanera  rápida<br> 
                        y eficaz.<br> 
                        También cuenta con un  régimen de seguridad a la<br>
                        información   de  los  Usuarios,  Propietarios y<br> 
                        pacientes   garantizando   la  confidencialidad,<br> 
                        integridad y disponibilidad de la información.<br>
                        </font></p>
                       <h1><img src="<?php echo URL_ASSETS ?>imagenes/asmaxi.PNG" width="10%" ></h1>
                        <h2><p><font size="5"> Fue desarrollado por la empresa ASMAXI S.A.S<br>
                        conformada por aprendices SENA en el programa de<br> 
                        formacion  Tecnologo en Analisis y Desarrollo de<br>
                        Sistemas de Informacion.</font></p></h2>
                        <h2>¡¡Derechos Reserbados!!</h2>
            </h2>
            </div>
            
        </header>
    </div>