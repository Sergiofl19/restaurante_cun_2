<!DOCTYPE html>
<html>
<head>
	<title></title>    
	<!--metas de escritorio-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content="Circle Hover Effects with CSS Transitions" />
    <meta name="keywords" content="circle, border-radius, hover, css3, transition, image, thumbnail, effect, 3d" />
    <meta name="author" content="Codrops" />
    <!--llama a los estilos bootstrap-->
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo URL_ASSETS ?>bootstrap/js/bootstrap.min.js"></script>
    <!--llama a la ibreria jquery-->
    <script src="<?php echo URL_ASSETS ?>js/jquery.js"></script>
    <!--link de los escritorios-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/demo.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/common.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/menuV.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/menuA.css" />
     <!--sub demo--> 
    <script src="<?php echo URL_ASSETS ?>js/SUBmodernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/SUBnormalize.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/SUBdemo.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/SUBcomponent2.css" />    
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/Bdefault.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/Bcomponent.css" />
    <script src="<?php echo URL_ASSETS ?>js/Bmodernizr.custom.js"></script>
    <script src="<?php echo URL_ASSETS ?>js/modernizr.custom.79639.js"></script> 
        <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-7243260-2']);
    _gaq.push(['_trackPageview']);
    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    </script> 

        <!--script del esctritorio auxiliar-->
    
    <script src="<?php echo URL_ASSETS ?>js/nuevaMascota.js"></script>
    <script src="<?php echo URL_ASSETS ?>js/especieNueva.js"></script>
    <script src="<?php echo URL_ASSETS ?>js/modificarContraseña.js"></script>
</head>
<body>