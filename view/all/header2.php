<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Animated Books with CSS 3D Transforms" />
		<meta name="keywords" content="book, 3d, interactive, animated, 3d transform, css, web design" />
		<meta name="author" content="Marco Barría for Codrops" />
	<title></title>
	<link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/normalizeAD.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/demoAD.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_ASSETS ?>css/bookAD.css" />
    <script src="<?php echo URL_ASSETS ?>js/modernizr.customAD.js"></script>
    
</head>
<body>