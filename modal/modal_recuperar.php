
  <!-- Modal de recuperar contraseña-->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Recuperar password</h4>
          </div>
       <div class="modal-body"> 
        <form id="loginform" class="form-horizontal" role="form" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" autocomplete="off">
        
              <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <label for="email">Digita el correo asociado</label>
                <input id="email" type="email" class="form-control" name="email" placeholder="email" required>                                    
              </div>

                <div class="modal-footer">
                 <button id="btn-login" type="submit" class="btn btn-success">Enviar</a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
        </form>
      </div>
    </div>
  </div> 
  <!--Fin Modal recuperar contraseña-->