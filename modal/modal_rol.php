<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Rol</h4>
      </div>

        <form action="?c=Index&m=saveRol" method="POST" name="documento">
          <div class="modal-body">
            <label for="documento">Nombre del rol</label>
            <input type="text"  class="form-control" name="nombreRol" id="documento" required>
            <p id="validardocumento"></p>
          </div>
    
          <div class="modal-footer">
            <input type="submit" class="btn btn-info" value="Registrar" name="Enviar" >
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar <span class="glyphicon glyphicon-eye-close"> </span></button>
          </div>
          
        </form>     
    </div>  
  </div>
</div>