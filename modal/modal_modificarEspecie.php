 
<div class="modal fade" id="myModal1" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modificar especie</h4>
      </div>

        <form action="?c=Auxiliar&m=guardarEspecie" method="POST" name="raza">
          <div class="modal-body">
            <input type="hidden" name="idEspecie" />
            <label for="nombre">Nombre de la especie</label>
            <input type="text"  class="form-control" name="nomEspecie" id="nombre" onkeyup="agregar();"  ><p id="validarNombre"></p>
          </div>
    
          <div class="modal-footer">
 			<button  type="submit" class="btn btn-info">Guardar <span class="glyphicon glyphicon-floppy-disk"> </span></button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar <span class="glyphicon glyphicon-eye-close"> </span></button>
          </div>
          
        </form>     
    </div>  
  </div>
</div>