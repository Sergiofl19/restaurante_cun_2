<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Tipo de documento</h4>
      </div>

        <form action="?c=Index&m=guardarTipoDocumento" method="POST" name="documento">
          <div class="modal-body">
            <label for="documento">Nombre tipo documento</label>
            <input type="text"  class="form-control" name="documento" id="documento" onkeyup="agregar();" required>
            <p id="validardocumento"></p>
          </div>
    
          <div class="modal-footer">
            <input type="submit" class="btn btn-info" value="Registrar" name="Enviar" >
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar <span class="glyphicon glyphicon-eye-close"> </span></button>
          </div>
          
        </form>     
    </div>  
  </div>
</div>