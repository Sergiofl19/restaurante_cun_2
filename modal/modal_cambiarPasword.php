<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Restauración de contraseña</h4>
        </div>
          <div class="modal-body"> 
          
            <form action="actualizarClave.php" method="POST" name="contacto_frm">           

                <!-- Text input Contraseña-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Nueva contraseña</label> 
                    <div class="input-group"> 
                      <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                      <input type="password" class="form-control" placeholder="Password" name="clave" id="clave" onkeyup="validarContrasena();">
                    </div>
                </div> 

                <!-- Text input Repita Contraseña-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Confirme contraseña</label> 
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                        <input type="password" class="form-control" placeholder="Password" id="reClave" onkeyup="validarContrasena();">
                    </div>
                </div> <p id="reClaver" style="color: red;"></p>
          
              <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal" name="bEnviar">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </form>
          </div>
      </div>
  </div>
</div>  