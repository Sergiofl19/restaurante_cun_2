create database restaurante;
use restaurante;
create table tipo_documento(
    documento varchar(4) not null primary key
);

create table rol(
    nombreRol varchar(50) not null primary key
);

create table persona(
    idPersona int primary key not null auto_increment,
    nombre varchar(50) not null,
    usuario varchar(10) not null,
    clave varchar(50) not null,
    documento varchar(4) not null,
    numero varchar(50) not null,
    comision int not null,
    rol varchar(50) not null,
    foreign key(documento) references tipo_documento(documento),
    foreign key(rol) references rol(nombreRol)
);

create table precio(
    id_precio int not null primary key auto_increment,
    precio int
);

create table carta(
    id_plato int primary key not null auto_increment,
    plato text,
    id_precio int not null,
    tipo text,
    foreign key(id_precio) references precio(id_precio)
);

create table pedidos(
    id_pedido int not null primary key auto_increment,
    id_plato int not null,
    id_camarero int not null,
    observacion text,
    fecha DATE,
    foreign key(id_camarero) references persona(idPersona),
    foreign key(id_plato) references carta(id_plato)
);

create table mesa(
    id_mesa int primary key not null auto_increment,
    id_Pedido int not null,
    reservada int,
    liberar int,
    id_camarero int not null,
    foreign key(id_Pedido) references pedidos(id_pedido),
    foreign key(id_camarero) references persona(idPersona) 
);

create table facturacion(
    id_factura int primary key not null auto_increment,
    idPersona int not null,
    id_pedido int not null,
    foreign key(id_Pedido) references pedidos(id_pedido),
    foreign key(idPersona) references persona(idPersona)
);

insert into tipo_documento values('CC');
insert into rol values('administrador');
insert into persona (nombre, usuario, clave, documento, numero, comision, rol) values('Andres gaince', 'andres','1234', 'CC', '1007616822', 4000, 'administrador');