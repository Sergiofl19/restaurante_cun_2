function contrasena()
{
	var clave=document.getElementById("clave");
	var rclave=document.getElementById("reClave"); 

 		if (!clave.value && !rclave.value) {
			document.getElementById("reClaver").innerHTML="Los campos contraseñas son requeridos";
			clave.focus();
			return false;	
		} 
		else if (clave.value != rclave.value) {
			document.getElementById("reClaver").innerHTML="Las claves no coinciden";
			return false;	
		}else{
		document.getElementById('reClaver').innerHTML="";
		return true;	
	}
}
function validarNombre(){
	var exNombre=/^[a-zA-ZáćéįóúÿýżźñÉÓÚÑ\s]{10,45}$/;
	var nombre=document.getElementById("nombre");

		if (!nombre.value) {
			document.getElementById("nombrer").innerHTML="El campo nombre es requerido";
			nombre.focus();
			return false;

		}
		else if (!exNombre.exec(nombre.value)) {
			document.getElementById("nombrer").innerHTML="El campo nombre solo acepta letras";
			nombre.focus();
			return false;
		}else{
			document.getElementById("nombrer").innerHTML="";
			return true;
		}
}

function validarUsuario(){
	var exUsu=/^[a-zA-ZáćéįóúÿýżźñÉÓÚÑ\s]{4,20}$/;
	var usuario=document.getElementById("usuario");

	    if (!usuario.value) {
			document.getElementById("usuarior").innerHTML="El campo usuario es requerido";
			usuario.focus();
			return false;
		}else if (usuario.value.length < 4) {
			document.getElementById("usuarior").innerHTML="El campo usuario minimo debe tener 4 caracteres";
			usuario.focus();
			return false;
		}else if (usuario.value.length > 20) {
			document.getElementById("usuarior").innerHTML="El campo usuario maximo debe tener 20 caracteres";
			usuario.focus();
			return false;
		}
		else if (!exUsu.exec(usuario.value)) {
			document.getElementById("usuarior").innerHTML="El campo nombre solo acepta letras";
			usuario.focus();
			return false;
		}else{
			document.getElementById("usuarior").innerHTML="";
			return true;
		} 

}

function validarDocumento(){
	var rol=document.getElementById("documento"); 

	if (!rol.value || rol.value==0) {
		document.getElementById("documentor").innerHTML="Seleccione un tipo de documento";
		rol.focus();
		return false;	
	}else{
		document.getElementById("documentor").innerHTML="";
		return true;
	}
}
function validarNumero(){ 
	var nombre=document.getElementById("numero");

		if (!nombre.value) {
			document.getElementById("numeror").innerHTML="El campo numero es requerido";
			nombre.focus();
			return false;

		} else{
			document.getElementById("numeror").innerHTML="";
			return true;
		}
}

function validarComision(){ 
	var nombre=document.getElementById("comision");

		if (!nombre.value) {
			document.getElementById("comisionr").innerHTML="El campo comision es requerido";
			nombre.focus();
			return false;

		} else{
			document.getElementById("comisionr").innerHTML="";
			return true;
		}
}

function validarRol(){
		var rol=document.getElementById("rol"); 

		if (!rol.value || rol.value==0) {
			document.getElementById("rolr").innerHTML="Seleccione un rol";
			rol.focus();
			return false;	
		}else{
			document.getElementById("rolr").innerHTML="";
			return true;
		}
}
		
function validarForm() {
	if (validarNombre()&&validarUsuario()&&contrasena()&&validarRol()&&validarDocumento()&&validarNumero()&&validarComision()) {

			alert("El formulario se ha enviado Exitosamente");
			document.contacto_frm.submit(); 	
	}	
}
window.onload= function()
{
	var jenviar;
	jenviar= document.contacto_frm.bEnviar;
	jenviar.onclick = validarForm;
}
