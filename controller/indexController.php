<?php 
class indexController{
private $model;
private $especie;
private $camarero;
private $modelNuevaHistoria;

	public function __construct(){

		try {
			$this->model= new index();
			// $this->especie= new especie();
			$this->camarero=new Camareros();
			// $this->modelNuevaHistoria=new nuevaHistoria();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	//PAGINA DE INICIO
	public function index(){

		require_once('view/all/header.php');
		require_once('view/index/index.php');
		require_once('view/all/footer.php');
	}

	public function iniciarSesion(){
		
		$this->model= new index();
		foreach ($this->model->inicio($_POST['usuario']) as $k) {}
		if($k->nombreRol=="auxiliar"){
			header('location:?c=Index&m=indexA');

		}else if($k->nombreRol=="administrador"){
		 header('location:?c=Index&m=indexV');
		}else{
			echo 'Error al entrar a la base de datos';
		}
	}

	public function registrarUsuario(){
		$this->model=new index();
		$this->model->registrarUsuarios($_REQUEST['nombre'],$_REQUEST['usuario'],$_REQUEST['clave'],$_REQUEST['documento'],$_REQUEST['numero'],$_REQUEST['comision'],$_REQUEST['rol']);
		header('location:?c=Index&m=indexV');
	}
	public function guardarTipoDocumento(){
		$this->model= new index();
		$this->model->saveTipoDocumento($_REQUEST['documento']);
		header('Location: ?c=Index&m=index2V');
	}
	public function deleteTipoDocumento(){
		$this->model= new index();
		$this->model->deleteTipoDocumento($_REQUEST['documento']);
		header('location:?c=Index&m=index2V');
	}
	public function getByIdTipoDocumento(){
		$this->model=new index();
		if (isset($_REQUEST['documento'])) {
			$this->model->getByIdTipoDocumento($_REQUEST['documento']);
		}
		require_once('view/all/header1.php');
		require_once('view/escritorio/veterinario/updateTipoDocumento.php');
		require_once('view/all/footer.php');

	}
	public function modificarEspecie(){ 
		$this->especie= new especie();
		$this->especie->editarEspecie($_REQUEST['idEspecie'],$_REQUEST['nomEspecie']);
		header('Location: ?c=Index&m=index3');
	}

	public function deleteRol(){
		$this->model= new index();
		$this->model->deleteRol($_REQUEST['nombreRol']);
		header('location:?c=Index&m=vistaRol');
	}

	public function saveRol(){
		$this->model= new index();
		$this->model->saveRol($_REQUEST['nombreRol']);
		header('Location: ?c=Index&m=vistaRol');
	}

	//Escritorio auxiliar con sus respectivas funcionalidades
	public function indexA(){
		require_once('view/all/header1.php');
		require_once('view/escritorio/auxiliar/index.php');
		require_once('view/all/footer.php');
	}
	public function index1(){
		require_once('view/all/header5.php');
		require_once('view/escritorio/auxiliar/index1.php');
		require_once('view/all/footer1.php');
	}
	public function index2(){
		//require_once('view/all/header5.php');
		require_once('view/escritorio/auxiliar/index2.php');
		//require_once('view/all/footer1.php');
	}
	public function index3(){
		require_once('view/all/header1.php');
		require_once('view/escritorio/auxiliar/index3.php');
		require_once('view/all/footer.php');
	}
	public function index4(){
		require_once('view/all/header1.php');
		require_once('view/escritorio/auxiliar/index4.php');
		require_once('view/all/footer.php');
	}
	public function index4A(){
		require_once('view/all/header1.php');
		require_once('view/escritorio/auxiliar/index4A.php');
		require_once('view/all/footer.php');
	}
	public function index5(){
		require_once('view/all/header5.php');
		require_once('view/escritorio/auxiliar/index5.php');
		require_once('view/all/footer.php');
	}
	public function index5A1(){
		require_once('view/all/header5.php');
		require_once('view/escritorio/auxiliar/index5A1.php');
		require_once('view/all/footer.php');
	}
	public function index6(){
		require_once('view/all/header2.php');
		require_once('view/escritorio/auxiliar/index6.php');
		require_once('view/all/footer.php');
	}
	public function index7(){
		require_once('view/all/header3.php');
		require_once('view/escritorio/auxiliar/index7.php');
		require_once('view/all/footer.php');
	}
	//escritorio veterinario con sus respectivas funcionalidades
	public function indexV(){
		require_once('view/all/header1.php');
		require_once('view/escritorio/veterinario/index.php');
		require_once('view/all/footer.php');
	}
	public function index2V(){
		require_once('view/all/header1.php');
		require_once('view/escritorio/veterinario/index2.php');
		require_once('view/all/footer.php');
	}
	public function vistaRol(){
		require_once('view/all/header1.php');
		require_once('view/escritorio/veterinario/roles.php');
		require_once('view/all/footer.php');
	}
	public function index3V(){
		require_once('view/all/header1.php');
		require_once('view/escritorio/veterinario/index3.php');
		require_once('view/all/footer.php');
	}
	public function index4V(){
		require_once('view/all/header2.php');
		require_once('view/escritorio/veterinario/index4.php');
		require_once('view/all/footer.php');
	}
	public function index5V(){
		require_once('view/all/header3.php');
		require_once('view/escritorio/veterinario/index5.php');
		require_once('view/all/footer.php');
	} 
    //opciones de la clase raza
		public function guardarRaza(){
		$alm= new raza();
		$alm->idRaza=$_REQUEST['idRaza'];
		$alm->nomRaza=$_REQUEST['nomRaza'];
		$alm->idRaza>0

		?$this->raza->registrarRaza($alm)
		:$this->raza->editarRaza($alm);
		header('location:?c=Index&m=index4');
	}
	public function eliminarRaza(){
		$this->raza= new raza();
		$this->raza->eliminarRaza($_REQUEST['idRaza']);
		header('location:?c=Index&m=index4');
	}
	public function crudRaza(){
			$this->raza=new raza();
			if (isset($_REQUEST['idRaza'])) {
				$this->raza->obtenerRaza($_REQUEST['idRaza']);
			}
			require_once('view/all/header1.php');
			require_once('view/escritorio/auxiliar/index4A.php');
			require_once('view/all/footer.php');
	}
	public function modificarRaza(){ 
			$this->raza= new raza();
	    	$this->raza->editarRaza($_REQUEST['idRaza'],$_REQUEST['nomRaza']);
        	header('Location: ?c=Index&m=index4');
	} 
}
 ?>